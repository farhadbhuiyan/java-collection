import java.util.ArrayList;

public class IsEmpty{
	
	public static void main(String[] args){
		
		ArrayList<String> colors = new ArrayList<String>();
		
		colors.add("Green");
		colors.add("Red");
		colors.add("Yellow");
		colors.add("Purple");
		colors.add("Indigo");
		
		boolean isEmpty = colors.isEmpty();
		
		if(isEmpty){
			System.out.println("Opps! The list is empty.");
		} else{
			System.out.println("The list contain elements");
		}
		
	}
	
}