import java.util.ArrayList;
import java.util.List;

public class ExtractPortion{
	
	public static void main(String[] args){
		
		ArrayList<String> colors = new ArrayList<String>();
		
		colors.add("Green");
		colors.add("Red");
		colors.add("Yellow");
		colors.add("Purple");
		colors.add("Indigo");
		
		//Before extraction
		System.out.println("Before Extraction:\n");
		System.out.println(colors);
		
		List<String> subList =  colors.subList(2, 4);
		
		//After extraction
		System.out.println("\nAfter Extraction:\n");
		System.out.println(subList);
		
	}
	
}