import java.util.ArrayList;

public class CopyList{
	
	public static void main(String[] args){
		
		ArrayList<String> colors = new ArrayList<String>();
		
		colors.add("Green");
		colors.add("Red");
		colors.add("Yellow");
		colors.add("Purple");
		colors.add("Indigo");
		
		//Original List
		System.out.println("Original List: \n");
		System.out.println(colors);
		
		ArrayList<String> copyColors = new ArrayList<String>();
		
		copyColors = (ArrayList) colors.clone();
		
		//Copied list
		System.out.println("\nCopied list: \n");
		System.out.println(copyColors);
		
	}
	
}