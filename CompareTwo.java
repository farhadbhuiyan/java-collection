import java.util.ArrayList;

public class CompareTwo{
	
	public static void main(String[] args){
		
		ArrayList<String> colors = new ArrayList<String>();
		
		colors.add("Green");
		colors.add("Red");
		colors.add("Yellow");
		colors.add("Purple");
		colors.add("Indigo");
		
		//First Array
		System.out.println("First Array: \n");
		System.out.println(colors);
		
		ArrayList<String> colors2 = new ArrayList<String>();
		
		colors2.add("Green");
		colors2.add("Red");
		colors2.add("Yellow2");
		colors2.add("Purple");
		colors2.add("Indigo2");
		
		//Second array
		System.out.println("\nSecond Array: \n");
		System.out.println(colors2);
		
		ArrayList<String> commonColor = new ArrayList<String>();
		
		for(String color : colors){
			if(colors2.contains(color)){
				commonColor.add(color);
			}
		}
		
		//After compare
		System.out.println("\nAfter Compare:\n");
		System.out.println(commonColor);
		
	}
	
}