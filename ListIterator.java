import java.util.ArrayList;
import java.util.Iterator;

public class ListIterator{
	
	public static void main(String[] args){
		
		ArrayList<String> colors = new ArrayList<String>();
		
		colors.add("Green");
		colors.add("Red");
		colors.add("Yellow");
		colors.add("Purple");
		colors.add("Indigo");
		
		
		//Iterate using for loop
		System.out.println("Iterate using for loop\n");
		
		for(int i = 0; i<colors.size(); i++){
			System.out.println(colors.get(i));
		}
		
		
		//Iterate using enhanched for loop
		System.out.println("\n Iterate using enhanched for loop \n");
		
		for(String color : colors){
			System.out.println(color);
		}
		
		//Iterate using while loop
		System.out.println("\n Iterate using while loop \n");
		
		int counter = 0;
		
		while(counter < colors.size()){
			
			System.out.println(colors.get(counter));
			counter++;
		}
		
		//Iterate using java Iterator
		System.out.println("\n Iterate using java Iterator \n");
		
		Iterator<String> iterator =  colors.iterator();

		while(iterator.hasNext()){
			System.out.println(iterator.next());
		}
	}
	
}