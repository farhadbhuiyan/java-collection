import java.util.ArrayList;
import java.util.Collections;

public class SortedList{
	
	public static void main(String[] args){
		
		ArrayList<String> colors = new ArrayList<String>();
		
		colors.add("Green");
		colors.add("Red");
		colors.add("Yellow");
		colors.add("Purple");
		colors.add("Indigo");
		
		//Before sorted
		System.out.println("Before sorted: \n");
		System.out.println(colors);
		
		Collections.sort(colors);
		
		//After sorted
		System.out.println("\nAftr sorted: \n");
		System.out.println(colors);
		
	}
	
}