import java.util.ArrayList;
import java.util.Collections;

public class ReverseArray{
	
	public static void main(String[] args){
		
		ArrayList<String> colors = new ArrayList<String>();
		
		colors.add("Green");
		colors.add("Red");
		colors.add("Yellow");
		colors.add("Purple");
		colors.add("Indigo");
		
		//Before reverse
		System.out.println("Before Reverse: \n");
		System.out.println(colors);
		
		Collections.reverse(colors);
		
		//After reserve
		System.out.println("\nAfter Reverse: \n");
		System.out.println(colors);
		
	}
	
}