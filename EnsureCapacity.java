import java.util.ArrayList;

public class EnsureCapacity{
	
	public static void main(String[] args){
		
		ArrayList<String> colors = new ArrayList<String>(5);
		
		colors.add("Green");
		colors.add("Red");
		colors.add("Yellow");
		colors.add("Purple");
		colors.add("Indigo");
		
		//Initial size
		System.out.println("Initial Cpacity: \n");
		System.out.println(colors);
		
		colors.ensureCapacity(10);
		
		colors.add("Green");
		colors.add("Red");
		colors.add("Yellow");
		colors.add("Purple");
		colors.add("Indigo");
		colors.add("Green");
		colors.add("Red");
		colors.add("Yellow");
		colors.add("Purple");
		colors.add("Indigo");
		
		//After increasing capacity
		System.out.println("\nAfter increasing capacity:\n");
		System.out.println(colors);
		
	}
	
}