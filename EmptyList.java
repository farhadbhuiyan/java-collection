import java.util.ArrayList;

public class EmptyList{
	
	public static void main(String[] args){
		
		ArrayList<String> colors = new ArrayList<String>();
		
		colors.add("Green");
		colors.add("Red");
		colors.add("Yellow");
		colors.add("Purple");
		colors.add("Indigo");
		
		//Before remove
		System.out.println("Before remove: \n");
		System.out.println(colors);
		
		colors.removeAll(colors);
		
		//After remove
		System.out.println("After remove:");
		System.out.println(colors);
		
	}
	
}