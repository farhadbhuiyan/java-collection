import java.util.ArrayList;
import java.util.Collections;

public class ShuffleElement{
	
	public static void main(String[] args){
		
		ArrayList<String> colors = new ArrayList<String>();
		
		colors.add("Green");
		colors.add("Red");
		colors.add("Yellow");
		colors.add("Purple");
		colors.add("Indigo");
		
		//Before Shuffle
		System.out.println("Before Shuffle: \n");
		System.out.println(colors);
		
		Collections.shuffle(colors);
		
		//After Shuffle
		System.out.println("\nAfter Shuffle: \n");
		System.out.println(colors);
		
	}
	
}