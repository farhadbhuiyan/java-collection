import java.util.ArrayList;

public class JoinTwo{
	
	public static void main(String[] args){
		
		ArrayList<String> colors = new ArrayList<String>();
		
		colors.add("Green");
		colors.add("Red");
		colors.add("Yellow");
		colors.add("Purple");
		colors.add("Indigo");
		
		
		//First array list
		System.out.println("\nFirst Array List:\n");
		System.out.println(colors);
		
		ArrayList<String> colors2 = new ArrayList<String>();
		
		colors2.add("Green2");
		colors2.add("Red2");
		colors2.add("Yellow2");
		colors2.add("Purple2");
		colors2.add("Indigo2");
		
		//Second array list
		System.out.println("\nSecond Array List:\n");
		System.out.println(colors2);
		
		ArrayList<String> combinedList = new ArrayList<String>();
		
		combinedList.addAll(colors);
		combinedList.addAll(colors2);
		
		
		
		//After Combining
		System.out.println("\nCombined Array List:\n");
		System.out.println(combinedList);
		
	}
	
}