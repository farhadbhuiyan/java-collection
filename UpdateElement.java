import java.util.ArrayList;
import java.util.Iterator;

public class UpdateElement{
	
	public static void main(String[] args){
		
		ArrayList<String> colors = new ArrayList<String>();
		
		colors.add("Green");
		colors.add("Red");
		colors.add("Yellow");
		colors.add("Purple");
		colors.add("Indigo");
		
		//Before replacement
		System.out.println("Before Replacement \n");
		System.out.println(colors);
		
		//Replace element at index 2 with Magenta
		String fourthElement = colors.set(2, "Magenta");
		
		//After replacement
		System.out.println("\nAfter Replacement \n");
		System.out.println(colors); //Indigo
		
	}
	
}