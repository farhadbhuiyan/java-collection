import java.util.ArrayList;

public class RemoveThird{
	
	public static void main(String[] args){
		
		ArrayList<String> colors = new ArrayList<String>();
		
		colors.add("Green");
		colors.add("Red");
		colors.add("Yellow");
		colors.add("Purple");
		colors.add("Indigo");
		
		//Before remove
		System.out.println("Before Remove \n");
		System.out.println(colors);
		
		//Remove third element
		colors.remove(2);
		
		//After remove
		System.out.println("\nAfter remove \n");
		System.out.println(colors);
		
	}
	
}