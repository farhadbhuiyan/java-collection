import java.util.ArrayList;
import java.util.Iterator;

public class InsertAtFirst{
	
	public static void main(String[] args){
		
		ArrayList<String> colors = new ArrayList<String>();
		
		colors.add("Green");
		colors.add("Red");
		colors.add("Yellow");
		colors.add("Purple");
		colors.add("Indigo");
		
		//Before insertion
		System.out.println(colors);
		
		//Add a color at first index
		colors.add(0, "Indigo");
		
		//After insertion
		System.out.println(colors);
		
	}
	
}