import java.util.ArrayList;

public class SearchElement{
	
	public static void main(String[] args){
		
		ArrayList<String> colors = new ArrayList<String>();
		
		colors.add("Green");
		colors.add("Red");
		colors.add("Yellow");
		colors.add("Purple");
		colors.add("Indigo");
		
		//Search that the color Yellow is in the list or not 
		boolean isInList =  colors.contains("Yellow");
		
		if(isInList){
			System.out.println("Yehahoo! Elements found!");
		} else {
			System.out.println("Oopps! Not found!");
		}
		
	}
	
}