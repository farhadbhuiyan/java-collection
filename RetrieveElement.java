import java.util.ArrayList;
import java.util.Iterator;

public class RetrieveElement{
	
	public static void main(String[] args){
		
		ArrayList<String> colors = new ArrayList<String>();
		
		colors.add("Green");
		colors.add("Red");
		colors.add("Yellow");
		colors.add("Purple");
		colors.add("Indigo");
		
		//Retrieve an element form index 4
		String fourthElement = colors.get(4);
		
		//Fourth element
		System.out.println(fourthElement); //Indigo
		
	}
	
}