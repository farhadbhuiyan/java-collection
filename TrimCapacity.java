import java.util.ArrayList;

public class TrimCapacity{
	
	public static void main(String[] args){
		
		ArrayList<String> colors = new ArrayList<String>(15);
		
		colors.add("Green");
		colors.add("Red");
		colors.add("Yellow");
		colors.add("Purple");
		colors.add("Indigo");
		
		//After triming
		System.out.println("\nAfter Triming:\n");
		System.out.println(colors.size());
		
	}
	
}