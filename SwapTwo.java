import java.util.ArrayList;
import java.util.Collections;

public class SwapTwo{
	
	public static void main(String[] args){
		
		ArrayList<String> colors = new ArrayList<String>();
		
		colors.add("Green");
		colors.add("Red");
		colors.add("Yellow");
		colors.add("Purple");
		colors.add("Indigo");
		
		//Before swap
		System.out.println("Before Swap: \n");
		System.out.println(colors);
		
		Collections.swap(colors, 2, 4);
		
		//After swap
		System.out.println("\nAfter Swap: \n");
		System.out.println(colors);
		
	}
	
}